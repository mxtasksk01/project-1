# Bastion Host
resource "aws_instance" "bastion-server" {
  ami                         = "ami-0c2ab3b8efb09f272"
  instance_type               = "t2.micro"
  count                       = 1
  key_name                    = "my123key"
  vpc_security_group_ids      = [aws_security_group.bastion-server-sg.id]
  subnet_id                   = aws_subnet.my-public-web-subnet-1.id
  associate_public_ip_address = true
tags = {
    Name = "bastion-server"
  }
}

# EC2 instance in App Subnet
resource "aws_instance" "my-app-server-1" {
  ami                         = "ami-0c2ab3b8efb09f272"
  instance_type               = "t2.micro"
  count                       = 1
  key_name                    = "my123key"
  vpc_security_group_ids      = [aws_security_group.app-server-sg.id]
  subnet_id                   = aws_subnet.my-private-app-subnet-1.id
  associate_public_ip_address = false
  user_data                   = file("data.sh")
tags = {
    Name = "my-app-server-1"
  }
}

resource "aws_instance" "my-app-server-2" {
  ami                         = "ami-0c2ab3b8efb09f272"
  instance_type               = "t2.micro"
  count                       = 1
  key_name                    = "my123key"
  vpc_security_group_ids      = [aws_security_group.app-server-sg.id]
  subnet_id                   = aws_subnet.my-private-app-subnet-2.id
  associate_public_ip_address = false
  user_data                   = file("data.sh")
tags = {
    Name = "my-app-server-2"
  }
}